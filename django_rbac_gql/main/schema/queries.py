from django.contrib.auth.models import Group, Permission, ContentType
from graphene import List, Field, String, Boolean

from main.models import User, RootObject, NestedObject
from .types import (
    UserType,
    GroupType,
    PermissionType,
    NestedObjectType,
    RootObjectType,
)

from logging import getLogger

logger = getLogger(__name__)


class AuthorizationQuery:
    can_i = Boolean(action=String(), model=String())
    can = Boolean(
        user_id=String(),
        action=String(),
        model=String(),
    )

    def resolve_can_i(root, info, action, model):
        user = info.context.user
        assert user.is_authenticated, "Authentication required."

        return AuthorizationQuery.resolve_can(
            root, info, user.id, action, model
        )

    def resolve_can(root, info, user_id, action, model):
        user = User.objects.get(id=user_id)
        model = model.replace(" ", "").replace("_", "").lower()
        content_type = ContentType.objects.get(model=model)

        return user.has_perm(
            f"{content_type.app_label}."
            f"{action.replace(' ', '_').lower()}_"
            f"{content_type.model}"
        )


class UserQuery:
    users = List(UserType)
    user = Field(UserType, id=String())

    def resolve_users(root, info):
        return User.objects.filter(is_active=True)

    def resolve_user(root, info, id):
        return User.objects.get(id=id)


class GroupQuery:
    groups = List(GroupType)
    group = Field(GroupType, id=String())

    def resolve_groups(root, info):
        return Group.objects.all()

    def resolve_group(root, info, id):
        return Group.objects.get(id=id)


class PermissionQuery:
    permissions = List(PermissionType)
    permission = Field(PermissionType, id=String())

    def resolve_permissions(root, info):
        return Permission.objects.all()

    def resolve_permission(root, info, id):
        return Permission.objects.get(id=id)


class RootObjectQuery:
    root_objects = List(RootObjectType)
    root_object = Field(RootObjectType, id=String())

    def resolve_root_objects(root, info):
        return RootObject.objects.filter(is_active=True)

    def resolve_root_object(root, info, id):
        return RootObject.objects.get(id=id)


class NestedObjectQuery:
    nested_objects = List(NestedObjectType)
    nested_object = Field(NestedObjectType, id=String())

    def resolve_nested_objects(root, info):
        return NestedObject.objects.filter(is_active=True)

    def resolve_nested_object(root, info, id):
        return NestedObject.objects.get(id=id)
