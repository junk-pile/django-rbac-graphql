from django.contrib.auth.models import Group, Permission
from django.http import Http404, HttpResponseBadRequest
from graphene import Mutation, Int, String, Boolean, Field

from main.models import User, RootObject, NestedObject
from .types import UserType, GroupType, RootObjectType, NestedObjectType

from logging import getLogger

logger = getLogger(__name__)


class UpdateUserMutation(Mutation):
    class Arguments:
        user_id = String()
        password = String(required=False)
        is_active = Boolean(required=False)
        username = String(required=False)
        first_name = String(required=False)
        last_name = String(required=False)
        email = String(required=False)
        name = String(required=False)

    user = Field(UserType)

    @classmethod
    def mutate(cls, root, info, user_id, **kwargs):
        usr = User.objects.filter(id=user_id)
        if info.context.user != usr.first() or not usr.exists():
            raise Http404("User not found.")
        usr.update(**kwargs)
        u = usr.first()
        if "password" in kwargs:
            logger.debug("Updated password.")
            u.set_password(kwargs["password"])
            u.save()
        return UpdateUserMutation(user=u)


class CreateUserMutation(Mutation):
    class Arguments:
        username = String()
        password = String()
        email = String()
        first_name = String(required=False)
        last_name = String(required=False)
        name = String(required=False)

    user = Field(UserType)

    @classmethod
    def mutate(cls, root, info, username, password, email, **kwargs):
        if info.context.user.is_authenticated:
            raise HttpResponseBadRequest("Cannot register while logged in.")
        usr = User.objects.create(username=username, email=email, **kwargs)
        usr.set_password(password)
        usr.save()
        return CreateUserMutation(user=usr)


class AddGroupPermissionMutation(Mutation):
    class Arguments:
        id = Int()
        permission_id = Int()

    group = Field(GroupType)

    @classmethod
    def mutate(cls, root, info, id, permission_id):
        group = Group.objects.get(id=id)
        permission = Permission.objects.get(id=permission_id)
        group.permissions.add(permission)
        return AddGroupPermissionMutation(group=group)


class AddGroupMutation(Mutation):
    class Arguments:
        name = String()

    group = Field(GroupType)

    @classmethod
    def mutate(cls, root, info, name):
        group = Group.objects.create(name=name)
        return AddGroupMutation(group=group)


class CreateRootObjectMutation(Mutation):
    class Arguments:
        owner_id = String()
        name = String()

    root_object = Field(RootObjectType)

    @classmethod
    def mutate(cls, root, info, owner_id, name):
        owner = User.objects.get(id=owner_id)
        root_object = RootObject.objects.create(owner=owner, name=name)
        return CreateRootObjectMutation(root_object=root_object)


class CreateNestedObjectMutation(Mutation):
    class Arguments:
        parent_id = String()
        name = String()
        is_hidden = Boolean(required=False)

    nested_object = Field(NestedObjectType)

    @classmethod
    def mutate(cls, root, info, parent_id, name, **kwargs):
        parent = RootObject.objects.get(id=parent_id)
        nested_object = NestedObject.objects.create(
            parent=parent, name=name, **kwargs
        )
        return CreateNestedObjectMutation(nested_object=nested_object)


class AddUserGroupMutation(Mutation):
    class Arguments:
        user_id = String()
        group_id = Int()

    user = Field(UserType)

    @classmethod
    def mutate(cls, root, info, user_id, group_id):
        user = User.objects.get(id=user_id)
        group = Group.objects.get(id=group_id)
        user.groups.add(group)
        return AddUserGroupMutation(user=user)
