from django.contrib.auth.models import Group, Permission, ContentType
from graphene_django import DjangoObjectType
from graphene import List, Field, String, Int

from main.models import User, NestedObject, RootObject


class ContentTypeType(DjangoObjectType):
    class Meta:
        model = ContentType
        fields = ("id", "app_label", "model")


class PermissionType(DjangoObjectType):
    content_type = Field(ContentTypeType)

    def resolve_content_type(self, info):
        return self.content_type

    class Meta:
        model = Permission
        fields = ("id", "name", "codename")


class GroupType(DjangoObjectType):
    permissions = List(PermissionType)
    permission = Field(PermissionType, id=Int())

    def resolve_permissions(self, info):
        return self.permissions.all()

    def resolve_permission(self, info, id):
        return self.permissions.get(id=id)

    class Meta:
        model = Group
        fields = ("id", "name")


class NestedObjectType(DjangoObjectType):
    # using the lambda here allows for specifying a type that hasn't been
    # declared yet (which is later in this file)
    parent = Field(lambda: RootObjectType)

    def resolve_parent(self, info):
        return self.parent

    class Meta:
        model = NestedObject
        fields = ("id", "name", "is_hidden", "is_active")


class RootObjectType(DjangoObjectType):
    owner = Field(lambda: UserType)
    children = List(NestedObjectType)
    child = Field(NestedObjectType, id=String())

    def resolve_children(self, info):
        return self.children.filter(is_active=True)

    def resolve_child(self, info, id):
        return self.children.get(id=id)

    def resolve_owner(self, info):
        return self.owner

    class Meta:
        model = RootObject
        fields = ("id", "name", "is_active")


class UserType(DjangoObjectType):
    root_objects = List(RootObjectType)
    root_object = Field(RootObjectType, id=String())

    def resolve_objects(self, info):
        return self.root_objects.filter(is_active=True)

    def resolve_object(self, info):
        return self.root_objects.get(id=id)

    class Meta:
        model = User
        exclude = ("password",)
