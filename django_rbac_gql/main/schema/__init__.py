from graphene import String, Schema, ObjectType, Field

from .types import UserType

from .queries import (
    AuthorizationQuery,
    UserQuery,
    GroupQuery,
    PermissionQuery,
    RootObjectQuery,
    NestedObjectQuery,
)
from .mutations import (
    UpdateUserMutation,
    CreateUserMutation,
    AddGroupPermissionMutation,
    AddGroupMutation,
    CreateRootObjectMutation,
    CreateNestedObjectMutation,
    AddUserGroupMutation,
)


class Query(
    AuthorizationQuery,
    UserQuery,
    GroupQuery,
    PermissionQuery,
    RootObjectQuery,
    NestedObjectQuery,
    ObjectType,
):
    hello = String(default_value="Hi!")
    current_user = Field(UserType)

    def resolve_current_user(root, info):
        if info.context.user.is_authenticated:
            return info.context.user
        else:
            return None


class Mutation(ObjectType):
    update_user = UpdateUserMutation.Field()
    create_user = CreateUserMutation.Field()
    add_group_permission = AddGroupPermissionMutation.Field()
    add_group = AddGroupMutation.Field()
    create_root_object = CreateRootObjectMutation.Field()
    create_nested_object = CreateNestedObjectMutation.Field()
    add_user_group = AddUserGroupMutation.Field()


schema = Schema(query=Query, mutation=Mutation)
