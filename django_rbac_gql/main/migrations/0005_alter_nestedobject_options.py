# Generated by Django 3.2.13 on 2022-05-28 22:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_alter_rootobject_owner'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='nestedobject',
            options={'permissions': [('view_hidden_nestedobject', 'View Hidden')], 'verbose_name_plural': 'nested objects'},
        ),
    ]
