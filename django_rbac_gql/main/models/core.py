from django.db import models
from django.contrib.auth.models import AbstractUser

from uuid import uuid4


class Base(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=32, unique=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class User(AbstractUser, Base):
    def name(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        db_table = "user"
        verbose_name_plural = "users"


class RootObject(Base):
    owner = models.ForeignKey(
        User, related_name="root_objects", on_delete=models.CASCADE
    )

    class Meta:
        db_table = "root_object"
        verbose_name_plural = "root objects"


class NestedObject(Base):
    is_hidden = models.BooleanField(default=False)
    parent = models.ForeignKey(
        RootObject, related_name="children", on_delete=models.CASCADE
    )

    class Meta:
        permissions = [("view_hidden_nestedobject", "View Hidden")]
        db_table = "nested_object"
        verbose_name_plural = "nested objects"
