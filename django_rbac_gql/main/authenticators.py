from django.contrib.auth.middleware import AuthenticationMiddleware
from binascii import Error as BinasciiError
from base64 import b64decode

from main.models import User

from logging import getLogger

logger = getLogger(__name__)


class BasicAuthMiddleware(AuthenticationMiddleware):
    def process_request(self, request):
        super().process_request(request)
        if (
            not request.user.is_authenticated
            and "HTTP_AUTHORIZATION" in request.META
        ):
            auth_header = request.META.get("HTTP_AUTHORIZATION")
            if " " not in auth_header:
                return
            auth_type, auth_enc = auth_header.split(" ")
            if auth_type.lower() != "basic":
                return
            try:
                auth_dec = b64decode(auth_enc).decode()
                if ":" not in auth_dec:
                    return
                username, password = auth_dec.split(":")

                user = User.objects.get(username=username)
                if user.check_password(password):
                    request.user = user
                return
            except BinasciiError:
                return
