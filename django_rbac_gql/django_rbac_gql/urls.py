from django.contrib import admin
from django.urls import path, include
from django.conf import settings

from graphene_django.views import GraphQLView

from urllib.parse import urljoin

urlpatterns = [
    path(urljoin(settings.BASE_URL, "admin/"), admin.site.urls),
    path(urljoin(settings.BASE_URL, "gql"), GraphQLView.as_view(graphiql=True)),
    path(settings.BASE_URL, include("jwt_auth.urls")),
]
