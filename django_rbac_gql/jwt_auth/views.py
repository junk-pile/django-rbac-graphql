from django.contrib.auth import authenticate, get_user_model
from django.views import View
from django.conf import settings
from django.http import JsonResponse

from logging import getLogger

from .auth import JWTifier

logger = getLogger(__name__)


def set_cookie(response, key, value, path="/"):
    response.set_cookie(
        key=key,
        value=value,
        path=path,
        samesite="Strict",
        httponly=settings.JWT_COOKIE_HTTPONLY,
        secure=settings.JWT_COOKIE_SECURE,
    )


class Login(View):
    def post(self, request):
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = (
            authenticate(username=username, password=password) or request.user
        )

        if user.is_authenticated:
            jwtifier = JWTifier(user)
            access, refresh = jwtifier.create_refresh()

            response = JsonResponse(
                {"message": "Authentication successful."}, status=200
            )

            set_cookie(response, settings.JWT_ACCESS_HEADER, f"Bearer {access}")
            set_cookie(
                response,
                "RefreshAuthorization",
                f"{refresh}",
                path=settings.JWT_REFRESH_PATH,
            )

            return response

        else:
            return JsonResponse(
                {"message": "Valid user credentials not provided."}, status=400
            )


class Logout(View):
    def post(self, request):
        if request.user.is_authenticated:
            response = JsonResponse({"message": "Session ended."}, status=200)
            set_cookie(response, settings.JWT_ACCESS_HEADER, "")
            set_cookie(response, f"Refresh{settings.JWT_ACCESS_HEADER}", "")
            return response
        else:
            return JsonResponse(
                {"message": "Must be authenticated to log out."}, status=401
            )


class Refresh(View):
    def post(self, request):
        try:
            # check for a valid refresh token
            token_str = request.META.get(
                f"HTTP_REFRESH{settings.JWT_ACCESS_HEADER}"
            ) or request.COOKIES.get(f"Refresh{settings.JWT_ACCESS_HEADER}")

            token = JWTifier.token_from_str(token_str)
            logger.debug(token)
            assert token is not None
            assert token["user"].is_active

            jwtifier = JWTifier(token["user"])
            access = jwtifier.create_access()

            response = JsonResponse(
                {"message": "Access token refreshed."}, status=200
            )
            set_cookie(response, settings.JWT_ACCESS_HEADER, f"Bearer {access}")
            return response
        except (AssertionError, ValueError):
            return JsonResponse(
                {"message": "Invalid refresh token."}, status=403
            )
